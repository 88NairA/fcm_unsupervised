function c24_out = nhl_func( w,c )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
str = sprintf('Arxh kwdika\n');
disp(str);
format long

[cm cn]=size(c);
c_rows=cm;
c_cols=cn;
array_size=c_cols;
[wm wn]=size(w);
w_rows=wm;
w_cols=wn;
sum_temp=0;
a=c;
temp_counter=0;
n=0.001;
g=0.98;
z=0;
while (z~=c_rows)
%z=5;% ALLAKSTE TIMH STO Z. GIA Z=2 THA DIALEKSOUME DIANYSMA EISODOY THN DEYTERH GRAMMH TOY PINAKA C ktl...
z=z+1;
w_update=split_sets_train(w,g,n,a(z,:));
while (temp_counter~=10)  % DEN EXW VALEI KAPOIO KRHTHRIO TERMATISMOU, MESA STH WHILE MPOREITE NA KATHORISETE POSES FORES 
    %NA TREKSEI O ALGORITHMOS GIA ENA DIANYSMA EISODOY
    temp_counter=temp_counter+1;
    for i=1:array_size
        for j=1:array_size
            if(i~=j)
                sum_temp=sum_temp+w_update(j,i)*(2*a(z,j)-1);%EDW EINAI TO LATHOS
            end
        end     
        a_post(i)=(2*a(z,i)-1)+sum_temp;
        a_post(i)=sigmoid_func(a_post(i),0,1);      
        sum_temp=0;
    end   
   % a_post(24)      %kathe fora ektypwnei to kainourgio dianysma poy prokyptei
    w_update=split_sets_train(w_update,g,n,a_post);
    a(z,:)=a_post;
end
fprintf('%i) %f\n',z,a_post(24))
%a_post(24)
temp_counter=0;
end

end

